#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cassert>
#include <cstring>

#define MAX 100000000

using namespace std;

int a[MAX];

long randint(long l,long u) {
  return l+rand()%(u-l+1);
}

int main(int argc,char **argv) {
  assert(argc==3);
  
  long m=atoi(argv[1]);
  long n=atoi(argv[2]);

  assert(n<MAX);

  if(m>n)
    m=n;

  memset(a,0,n);

  for(int i=0;i<n;++i) {
    a[i]=i;
  }

  for(int i=0;i<m;++i) {
    long r=randint(i,n-1);
    swap(a[i],a[r]);
    cout<<a[i]<<endl;
  }

  return 0;
}
