#include <cstdio>
#include <iostream>

using namespace std;

const int bitcount=32;
const long mx=10000000;
const int shift=5;


long ar[1+(mx/bitcount)];

void set(long i) {
  ar[i>>shift] |= (1<<i);
}

void clr(long i) {
  ar[i>>shift] &= ~(1<<i);
}

bool test(long i) {
  return (ar[i>>shift] & (1<<i));
}

int main() {
  long n;

  for(long i=0;i<mx;++i) {
    clr(i);
  }

  while(cin>>n) {
    set(n);
  }

  for(long i=0;i<mx;++i) {
    if(test(i))
      cout<<i<<endl;
  }

  return 0;
}
